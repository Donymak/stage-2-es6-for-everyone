import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter) {
    fighterElement.appendChild(createFighterImage(fighter))
    fighterElement.appendChild(createFighterDescription(fighter))
    console.log(fighter)
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

export function createFighterDescription(fighter) {
  const { name, attack, health, defense } = fighter;
  const descriptionElement = createElement( {
    tagName: 'div',
    className: `fighter-preview___description`,
  })
  descriptionElement.innerHTML =
       `<h2>Name: ${name}</h2>
        <h3>Health: ${health}</h3>
        <h3>Attack: ${attack}</h3>
        <h3>Defence: ${defense}</h3>`

  return descriptionElement
}
