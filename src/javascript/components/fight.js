import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
      const firstFighterStatus = createFighterStatus(firstFighter, "left");
      const secondFighterStatus = createFighterStatus(secondFighter, "right")
      console.log(firstFighterStatus,secondFighterStatus)

      handleAttackActions(firstFighterStatus, secondFighterStatus, resolve);
      hanldeCritAttack(firstFighterStatus, secondFighterStatus, resolve);
      handleBlockState(firstFighterStatus,secondFighterStatus);
  });
}

export function createFighterStatus (fighter, side) {
    return {
      fighter,
      health: fighter.health,
      isBlocking: false,
      ableToCrit: true,
      indicator: document.getElementById(`${side}-fighter-indicator`)
    }
  }

export function getDamage(attacker, defender) {
  if ((getHitPower(attacker) - getBlockPower(defender)) > 0) {
    return getHitPower(attacker) - getBlockPower(defender)
  } else {
    return 0
  }
}

function handleBlockState (firstFighterStatus, secondFighterStatus) {

  document.addEventListener("keydown", event => {
    switch (event.code) {
      case controls.PlayerOneBlock: firstFighterStatus.isBlocking = true;
            break
      case controls.PlayerTwoBlock: secondFighterStatus.isBlocking = true
            break
    }
  })

  document.addEventListener("keyup", event => {
    switch (event.code) {
      case controls.PlayerOneBlock: firstFighterStatus.isBlocking = false
            break;
      case controls.PlayerTwoBlock: secondFighterStatus.isBlocking = false
            break
    }
  })
}

function handleAttackActions (firstFighterStatus, secondFighterStatus, resolve) {
  const attack = (attackerStatus, defenderStatus) => {
    if (!attackerStatus.isBlocking && !defenderStatus.isBlocking) {
      if(attackAction(attackerStatus, defenderStatus, getDamage) === "win") {
        resolve(attackerStatus.fighter)
      }
    }
  }
  document.addEventListener("keydown", event => {
    switch (event.code) {
      case controls.PlayerOneAttack: attack(firstFighterStatus, secondFighterStatus);
            break;
      case controls.PlayerTwoAttack: attack(secondFighterStatus, firstFighterStatus);
            break;
    }
  });
}

function hanldeCritAttack(firstFighterStatus, secondFighterStatus, resolve) {

  const pressedKeys = new Set();

  const isCombinationOk = combination => combination.every(key => pressedKeys.has(key));

  function setCritCooldown (attackerStatus) {
    attackerStatus.ableToCrit = false;
    setTimeout(() => attackerStatus.ableToCrit = true, 10000)
  }

  const tryAttack = (attackerStatus, defenderStatus, combination) => {
    if (!attackerStatus.isBlocking && attackerStatus.ableToCrit && isCombinationOk(combination)) {
      setCritCooldown(attackerStatus);
      pressedKeys.clear();
      if (attackAction(attackerStatus, defenderStatus, getCritDamage) === "win" ) {
        resolve(attackerStatus.fighter)
      }
    }
  }
  const keyDownHandler = event => {
    pressedKeys.add(event.code)
    tryAttack(firstFighterStatus,secondFighterStatus, controls.PlayerOneCriticalHitCombination);
    tryAttack(secondFighterStatus, firstFighterStatus, controls.PlayerTwoCriticalHitCombination);
  }

  document.addEventListener("keydown", keyDownHandler);
  document.addEventListener("keyup", event => pressedKeys.delete(event.code))
}

function attackAction (attackerStatus, defenderStatus, calcDamage) {
  const { fighter: attacker } = attackerStatus;
  const { fighter: defender } = defenderStatus;

  const damage = calcDamage(attacker,defender);
  const possibleDamage = Math.min(defenderStatus.health, damage);
  defenderStatus.health -= possibleDamage;
  defenderStatus.indicator.style.width = `${100 / defender.health * defenderStatus.health}%`;

  return defenderStatus.health === 0 ? "win" : "blocked";
}

export function getHitPower(fighter) {
  return fighter.attack * criticalHitChance(1, 2)
}

export function getBlockPower(fighter) {
  return fighter.defense * dodgeChance(1, 2)
}

function criticalHitChance(min, max) {
  return Math.random() * (max - min)  + min;
}

function dodgeChance(min, max) {
  return Math.random() * (max - min)  + min;
}

function getCritDamage(attacker) {
  return attacker.attack * 2
}